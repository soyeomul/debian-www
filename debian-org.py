#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# 파일명: debian-org.py

import json

# 데비안 프로젝트 조직 체계 용례
# 참고문헌: ≪데비안 헌법≫
# <URL:https://www.debian.org/devel/constitution.en.html>

DPL = (
    "Debian Project Leader",
    "데비안 프로젝트 -- 의장(議長)",
    "leader@debian.org",
)

CTTE = (
    "Debian Technical Committee",
    "데비안 기술 위원회 -- 위원장(委員長)",
    "debian-ctte@lists.debian.org",
)

DPS = (
    "Debian Project Secretary",
    "데비안 프로젝트 사무국 -- 사무국장(事務局長)",
    "secretary@debian.org",
)

KEY = ["DPL", "CTTE", "DPS",]
VALUE = [DPL, CTTE, DPS,]

dd = dict(zip(KEY, VALUE))

dj = json.dumps(dd, indent=8, ensure_ascii=False)

print(dj)

# 편집: GNU Emacs 26.3 (Ubuntu 18.04)
# 최초 작성일: 2019년 5월 9일
# 마지막 갱신: 2020년 9월 29일
